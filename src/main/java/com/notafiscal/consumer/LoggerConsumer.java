package com.notafiscal.consumer;


import com.notafiscal.model.Logger;
import com.notafiscal.service.LoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class LoggerConsumer {

    @Autowired
    LoggerService service;

    @KafkaListener(topics = "spec2-jonathan-roberto-3", groupId = "Novo-Grupo-0")
    public void receber(@Payload Logger logger) {

        //Atualizar Nota fiscal
        service.logar(logger);

        System.out.println("Log recebido : "
                + "\nId: " + logger.getUsuarioName()
                + "\nData: " + logger.getDataHora()
                + "\nIdentidade: " + logger.getIdentidade()
                + "\nTipo: " + logger.getTipoLog().getDescricao());
    }

}
