package com.notafiscal.service;

import com.notafiscal.model.Logger;
import com.notafiscal.model.TipoLog;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Service
public class LoggerService {

    public void logar(Logger logger) {

        try {
            FileWriter arquivo = new FileWriter("/home/a2/kafka-consumer/notafiscal/log-nota.txt", true);

            PrintWriter gravarArquivo = new PrintWriter(arquivo);

            gravarArquivo.append("\n"
                    +"["+ logger.getDataHora() + "] "
                    +"["+ logger.getTipoLog().getDescricao() + "]: "
                    + getDescricaolog(logger));

            gravarArquivo.flush();
            arquivo.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String getDescricaolog(Logger logger) {
        String descricao = null;
        if(logger.getTipoLog() == TipoLog.EMISSAO){
            descricao = logger.getUsuarioName() +" Acaba de pedir a emissão de uma NF da identidade "+logger.getIdentidade()+" no valor de R$ " +logger.getValor()+"\n";
        }else{
            descricao = logger.getUsuarioName() +" Acaba de pedir os dados das notas fiscais da identidade: "+ logger.getIdentidade()+"\n";
        }
        return descricao;
    }
}
